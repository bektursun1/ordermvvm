package com.bektursun.ordermvvm.utils

import android.content.Context
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources

fun Context.getDrawableExt(@DrawableRes drawableResId: Int) = AppCompatResources.getDrawable(this, drawableResId)