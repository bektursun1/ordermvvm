package com.bektursun.ordermvvm.data

import com.bektursun.ordermvvm.R
import com.bektursun.ordermvvm.data.model.Delivery
import com.bektursun.ordermvvm.data.model.Product
import com.bektursun.ordermvvm.data.model.ProductCategory
import com.bektursun.ordermvvm.data.model.ProductCategoryType

object MockedData {

    val homeDelivery = listOf(
        Delivery("Delivery", R.drawable.ic_delivery),
        Delivery("Pickup"),
        Delivery("Catering"),
        Delivery("Curbside")
    )

    val detailDelivery = listOf(
        Delivery("Burgers"),
        Delivery("Hot dogs"),
        Delivery("Sides"),
        Delivery("Desserts"),
        Delivery("Pizzas")
    )

    val takeawayProduct = listOf(
        Product(
            R.drawable.burger_first,
            "Burger Craze",
            "OPEN",
            4.6f,
            "American",
            "Delivery: FREE",
            "Minimum: $10",
            "Burgers",
            "10-15 min",
            604,
            true
        ), Product(
            R.drawable.ic_pizza,
            "Vegetarian Pizza",
            "OPEN",
            4.6f,
            "Italian",
            "Delivery: FREE",
            "Minimum: $10",
            "Burgers",
            "10-15 min",
            604,
            false
        )
    )

    val detailCategoryProduct = listOf(
        ProductCategory(
            R.drawable.category_first,
            "Beef Burger",
            "Cheddar cheese, guacamole, pico de Gallo and chipotle mayo",
            9.25f,
            ProductCategoryType.DEFAULT
        ),
        ProductCategory(
            R.drawable.category_first,
            "Cheese Burger",
            "Cheddar cheese, guacamole, pico de Gallo and chipotle mayo",
            9.25f,
            ProductCategoryType.SOLD_OUT
        ),
        ProductCategory(
            R.drawable.category_first,
            "Cheese Burger",
            "Cheddar cheese, guacamole, pico de Gallo and chipotle mayo",
            9.25f,
            ProductCategoryType.SPECIAL
        ),
        ProductCategory(
            R.drawable.category_first,
            "Cheese Burger",
            "Cheddar cheese, guacamole, pico de Gallo and chipotle mayo",
            9.25f,
            ProductCategoryType.DEFAULT
        )
    )
}