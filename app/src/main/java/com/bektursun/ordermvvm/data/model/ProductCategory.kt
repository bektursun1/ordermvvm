package com.bektursun.ordermvvm.data.model

import androidx.annotation.DrawableRes

data class ProductCategory(
    @DrawableRes
    val image: Int,
    val name: String,
    val description: String,
    val price: Float,
    val type: ProductCategoryType = ProductCategoryType.DEFAULT
)

enum class ProductCategoryType {
    SOLD_OUT, SPECIAL, DEFAULT
}