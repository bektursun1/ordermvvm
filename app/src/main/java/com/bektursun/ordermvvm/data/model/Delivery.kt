package com.bektursun.ordermvvm.data.model

import androidx.annotation.DrawableRes

data class Delivery(
    val name: String,
    @DrawableRes
    val icon: Int? = null
)