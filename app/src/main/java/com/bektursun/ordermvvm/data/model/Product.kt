package com.bektursun.ordermvvm.data.model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import kotlinx.parcelize.Parcelize

@Parcelize
data class Product(
    @DrawableRes
    val image: Int,
    val name: String,
    val status: String,
    val rating: Float,
    val country: String,
    val delivery: String,
    val price: String,
    val foodType: String,
    val cookingTime: String,
    val reviewCount: Int,
    val label: Boolean = false
): Parcelable