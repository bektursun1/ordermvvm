package com.bektursun.ordermvvm.core.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.reflect.KClass

abstract class CoreFragment<VB : ViewBinding, VM : CoreVM>(kClass: KClass<VM>) : Fragment() {

    private var _vb: VB? = null
    protected val vb: VB get() = _vb!!

    protected val vm: VM by viewModel(clazz = kClass)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _vb = createVB(inflater)
        return vb.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        subscribeToLiveData()
    }

    abstract fun createVB(inflater: LayoutInflater): VB

    open fun setupViews() {}

    open fun subscribeToLiveData() {}

    override fun onDestroyView() {
        super.onDestroyView()
        _vb = null
    }
}