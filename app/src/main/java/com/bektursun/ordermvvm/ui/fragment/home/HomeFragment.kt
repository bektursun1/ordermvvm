package com.bektursun.ordermvvm.ui.fragment.home

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import androidx.appcompat.content.res.AppCompatResources
import com.bektursun.ordermvvm.R
import com.bektursun.ordermvvm.core.ui.CoreFragment
import com.bektursun.ordermvvm.databinding.FragmentHomeBinding
import com.bektursun.ordermvvm.ui.fragment.home.adapter.CategoryTabAdapter
import com.bektursun.ordermvvm.ui.fragment.home.adapter.CategoryTabAdapter.Companion.CONVENIENCE_PAGE_INDEX
import com.bektursun.ordermvvm.ui.fragment.home.adapter.CategoryTabAdapter.Companion.GROCERY_PAGE_INDEX
import com.bektursun.ordermvvm.ui.fragment.home.adapter.CategoryTabAdapter.Companion.PHARMACY_PAGE_INDEX
import com.bektursun.ordermvvm.ui.fragment.home.adapter.CategoryTabAdapter.Companion.TAKEAWAYS_PAGE_INDEX
import com.bektursun.ordermvvm.ui.fragment.home.adapter.DeliveryCategoryAdapter
import com.bektursun.ordermvvm.utils.getDrawableExt
import com.google.android.material.tabs.TabLayoutMediator

class HomeFragment : CoreFragment<FragmentHomeBinding, HomeVM>(HomeVM::class) {

    private val deliveryAdapter = DeliveryCategoryAdapter()

    override fun createVB(inflater: LayoutInflater): FragmentHomeBinding =
        FragmentHomeBinding.inflate(inflater)

    override fun setupViews() {
        vb.rvDelivery.adapter = deliveryAdapter
        setupTabs()
    }

    override fun subscribeToLiveData() {
        vm.delivery.observe(viewLifecycleOwner, {
            it?.let { delivery ->
                deliveryAdapter.submitItems(delivery)
            }
        })
    }

    private fun setupTabs() {
        vb.vpCategory.adapter = CategoryTabAdapter(this)
        TabLayoutMediator(vb.tabsCategory, vb.vpCategory) { tab, position ->
            tab.text = getTabTitle(position)
            tab.icon = getIcon(position)
        }.attach()
    }

    private fun getTabTitle(position: Int): String? {
        return when (position) {
            TAKEAWAYS_PAGE_INDEX -> getString(R.string.takeaways)
            GROCERY_PAGE_INDEX -> getString(R.string.grocery)
            CONVENIENCE_PAGE_INDEX -> getString(R.string.convenience)
            PHARMACY_PAGE_INDEX -> getString(R.string.pharmacy)
            else -> null
        }
    }

    private fun getIcon(position: Int): Drawable? {
        return when (position) {
            TAKEAWAYS_PAGE_INDEX -> requireContext().getDrawableExt(R.drawable.ic_t)
            GROCERY_PAGE_INDEX -> requireContext().getDrawableExt(R.drawable.ic_t)
            CONVENIENCE_PAGE_INDEX -> requireContext().getDrawableExt(R.drawable.ic_t)
            PHARMACY_PAGE_INDEX -> requireContext().getDrawableExt(R.drawable.ic_t)
            else -> null
        }
    }

}