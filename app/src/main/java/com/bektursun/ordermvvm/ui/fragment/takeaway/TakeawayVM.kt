package com.bektursun.ordermvvm.ui.fragment.takeaway

import androidx.lifecycle.MutableLiveData
import com.bektursun.ordermvvm.core.ui.CoreVM
import com.bektursun.ordermvvm.data.MockedData
import com.bektursun.ordermvvm.data.model.Product

abstract class TakeawayVM : CoreVM() {
    abstract val product: MutableLiveData<List<Product>>
}

class TakeawayVMImpl : TakeawayVM() {

    override val product: MutableLiveData<List<Product>> = MutableLiveData(MockedData.takeawayProduct)

}