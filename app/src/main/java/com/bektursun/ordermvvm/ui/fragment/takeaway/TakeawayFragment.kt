package com.bektursun.ordermvvm.ui.fragment.takeaway

import android.view.LayoutInflater
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.bektursun.ordermvvm.R
import com.bektursun.ordermvvm.core.ui.CoreFragment
import com.bektursun.ordermvvm.data.model.Product
import com.bektursun.ordermvvm.databinding.FragmentTakeawayBinding
import com.bektursun.ordermvvm.ui.fragment.detail.ProductDetailFragment.Companion.PRODUCT_DATA_KEY
import com.bektursun.ordermvvm.ui.fragment.takeaway.adapter.ProductAdapter
import com.bektursun.ordermvvm.ui.fragment.takeaway.adapter.ProductListener

class TakeawayFragment : CoreFragment<FragmentTakeawayBinding, TakeawayVM>(TakeawayVM::class),
    ProductListener {

    private val productAdapter = ProductAdapter(this)

    override fun createVB(inflater: LayoutInflater): FragmentTakeawayBinding =
        FragmentTakeawayBinding.inflate(inflater)

    override fun setupViews() {
        vb.rvProduct.adapter = productAdapter
    }

    override fun subscribeToLiveData() {
        subscribeToProduct()
    }

    private fun subscribeToProduct() {
        vm.product.observe(viewLifecycleOwner, {
            it?.let { product ->
                productAdapter.submitItems(product)
            }
        })
    }

    override fun onProductClick(product: Product) {
        //val direction = TakeawayFragmentDirections.actionTakeawayFragmentToProductDetailFragment()
        findNavController().navigate(R.id.productDetailFragment, bundleOf(PRODUCT_DATA_KEY to product))
    }

    companion object {
        fun newInstance(): TakeawayFragment = TakeawayFragment()
    }
}