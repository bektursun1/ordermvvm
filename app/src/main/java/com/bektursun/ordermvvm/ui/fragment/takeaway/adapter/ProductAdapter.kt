package com.bektursun.ordermvvm.ui.fragment.takeaway.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bektursun.ordermvvm.R
import com.bektursun.ordermvvm.data.model.Product
import com.bektursun.ordermvvm.databinding.ItemProductBinding

class ProductAdapter(private val listener: ProductListener) : RecyclerView.Adapter<ProductAdapter.ProductVH>() {

    private var productList: List<Product> = emptyList()

    fun submitItems(items: List<Product>) {
        productList = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductVH =
        ProductVH.from(parent, listener)

    override fun onBindViewHolder(holder: ProductVH, position: Int) {
        holder.bind(productList[position])
    }

    override fun getItemCount(): Int = productList.size

    class ProductVH(private val binding: ItemProductBinding, private val listener: ProductListener) : RecyclerView.ViewHolder(binding.root) {

        fun bind(product: Product) {
            with(binding) {
                ivProduct.setImageResource(product.image)
                tvName.text = product.name
                tvStatus.text = product.status
                tvRatingCount.text = product.rating.toString()
                tvCountry.text = product.country
                tvDelivery.text = product.delivery
                tvDeliveryPrice.text = product.price
                tvProductType.text = product.foodType
                tvCookingTime.text = product.cookingTime
                tvReviewCount.text = product.reviewCount.toString()
                ivLabel.isVisible = product.label
            }
            binding.root.setOnClickListener {
                listener.onProductClick(product)
            }
        }

        companion object {
            fun from(parent: ViewGroup, listener: ProductListener): ProductVH {
                val inflater = LayoutInflater.from(parent.context)
                val binding = ItemProductBinding.inflate(inflater)
                return ProductVH(binding, listener)
            }
        }

    }
}

interface ProductListener {
    fun onProductClick(product: Product)
}