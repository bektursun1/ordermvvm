package com.bektursun.ordermvvm.ui.fragment.detail

import androidx.lifecycle.MutableLiveData
import com.bektursun.ordermvvm.core.ui.CoreVM
import com.bektursun.ordermvvm.data.MockedData
import com.bektursun.ordermvvm.data.model.Delivery
import com.bektursun.ordermvvm.data.model.Product
import com.bektursun.ordermvvm.data.model.ProductCategory

abstract class ProductDetailVM : CoreVM() {
    abstract val category: MutableLiveData<List<ProductCategory>>
    abstract val delivery: MutableLiveData<List<Delivery>>

    abstract val product: MutableLiveData<Product>
    abstract fun fetchProduct(product: Product)
}

class ProductDetailVMImpl : ProductDetailVM() {

    override val category: MutableLiveData<List<ProductCategory>> =
        MutableLiveData(MockedData.detailCategoryProduct)

    override val delivery: MutableLiveData<List<Delivery>> =
        MutableLiveData(MockedData.detailDelivery)

    override val product: MutableLiveData<Product> = MutableLiveData()

    override fun fetchProduct(product: Product) {
        this.product.value = product
    }

}