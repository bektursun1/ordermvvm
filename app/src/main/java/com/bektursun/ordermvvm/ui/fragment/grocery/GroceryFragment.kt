package com.bektursun.ordermvvm.ui.fragment.grocery

import android.view.LayoutInflater
import com.bektursun.ordermvvm.core.ui.CoreFragment
import com.bektursun.ordermvvm.databinding.FragmentGroceryBinding

class GroceryFragment : CoreFragment<FragmentGroceryBinding, GroceryVM>(GroceryVM::class) {

    override fun createVB(inflater: LayoutInflater): FragmentGroceryBinding =
        FragmentGroceryBinding.inflate(inflater)

    companion object {
        fun newInstance(): GroceryFragment = GroceryFragment()
    }
}