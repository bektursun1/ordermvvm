package com.bektursun.ordermvvm.ui.fragment.detail.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bektursun.ordermvvm.R
import com.bektursun.ordermvvm.data.model.ProductCategory
import com.bektursun.ordermvvm.data.model.ProductCategoryType
import com.bektursun.ordermvvm.databinding.ItemProductCategoryBinding

class ProductCategoryAdapter : RecyclerView.Adapter<ProductCategoryAdapter.ProductCategoryVH>() {

    private var productList: List<ProductCategory> = emptyList()

    fun submitItems(items: List<ProductCategory>) {
        productList = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductCategoryVH =
        ProductCategoryVH.from(parent)

    override fun onBindViewHolder(holder: ProductCategoryVH, position: Int) {
        holder.bind(productList[position])
    }

    override fun getItemCount(): Int = productList.size

    class ProductCategoryVH(private val binding: ItemProductCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(product: ProductCategory) {
            with(binding) {
                ivFood.setImageResource(product.image)
                tvFoodName.text = product.name
                foodDescription.text = product.description
                tvFoodPrice.text = product.price.toString()

                when(product.type) {
                    ProductCategoryType.SOLD_OUT -> {
                        Log.e("TAG", "SOLD OUT")
                        ivType.setImageResource(R.drawable.ic_sold_out)
                        //viewShadow.isVisible = true
                    }
                    ProductCategoryType.SPECIAL -> {
                        ivType.setImageResource(R.drawable.ic_special)
                        //viewShadow.isVisible = false
                    }
                    ProductCategoryType.DEFAULT -> {
                        //viewShadow.isVisible = false
                    }
                }
            }
        }

        companion object {
            fun from(parent: ViewGroup): ProductCategoryVH {
                val inflater = LayoutInflater.from(parent.context)
                val binding = ItemProductCategoryBinding.inflate(inflater)
                return ProductCategoryVH(binding)
            }
        }
    }


}