package com.bektursun.ordermvvm.ui.fragment.detail

import android.os.Bundle
import android.view.LayoutInflater
import androidx.navigation.findNavController
import com.bektursun.ordermvvm.core.ui.CoreFragment
import com.bektursun.ordermvvm.data.model.Product
import com.bektursun.ordermvvm.databinding.FragmentProductDetailBinding
import com.bektursun.ordermvvm.ui.fragment.detail.adapter.ProductCategoryAdapter
import com.bektursun.ordermvvm.ui.fragment.home.adapter.DeliveryCategoryAdapter

class ProductDetailFragment :
    CoreFragment<FragmentProductDetailBinding, ProductDetailVM>(ProductDetailVM::class) {

    private var product: Product? = null

    private val deliveryAdapter = DeliveryCategoryAdapter()
    private val categoryAdapter = ProductCategoryAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            product = it.getParcelable(PRODUCT_DATA_KEY)
        }
    }

    override fun createVB(inflater: LayoutInflater): FragmentProductDetailBinding =
        FragmentProductDetailBinding.inflate(inflater)

    override fun setupViews() {
        setupRV()
        setupProduct()
        vb.toolbar.setNavListener {
            it.findNavController().popBackStack()
        }
    }

    private fun setupRV() {
        vb.rvProduct.adapter = categoryAdapter
        vb.rvSubcategory.adapter = deliveryAdapter
        setupBasket()
    }

    private fun setupProduct() {
        product?.let {
            vm.fetchProduct(it)
        }
    }

    private fun setupBasket() {
        vb.checkout.showBasketIcon(false)
        vb.checkout.setPrice(8.8f)
        vb.checkout.setCount(2f)
    }

    override fun subscribeToLiveData() {
        subscribeToProduct()
        subscribeToProductCategory()
        subscribeToDelivery()
    }

    private fun subscribeToProduct() {
        vm.product.observe(viewLifecycleOwner, { product ->
            with(vb) {
                product?.let {
                    ivProduct.setImageResource(it.image)
                    tvName.text = it.name
                    tvRatingCount.text = it.rating.toString()
                    tvReviewCount.text = it.reviewCount.toString()
                    tvDelivery.text = it.delivery
                    tvDeliveryPrice.text = it.price
                    tvCountry.text = it.country
                    tvProductType.text = it.foodType
                    tvStatus.text = it.status
                }
            }
        })
    }

    private fun subscribeToDelivery() {
        vm.delivery.observe(viewLifecycleOwner, {
            it?.let { delivery ->
                deliveryAdapter.submitItems(delivery)
            }
        })
    }

    private fun subscribeToProductCategory() {
        vm.category.observe(viewLifecycleOwner, {
            it?.let { category ->
                categoryAdapter.submitItems(category)
            }
        })
    }

    companion object {
        fun newInstance(): ProductDetailFragment = ProductDetailFragment()

        const val PRODUCT_DATA_KEY: String = "product"
    }

}