package com.bektursun.ordermvvm.ui.fragment.home

import androidx.lifecycle.MutableLiveData
import com.bektursun.ordermvvm.core.ui.CoreVM
import com.bektursun.ordermvvm.data.MockedData
import com.bektursun.ordermvvm.data.model.Delivery

abstract class HomeVM : CoreVM() {
    abstract val delivery: MutableLiveData<List<Delivery>>
}

class HomeVMImpl : HomeVM() {
    override val delivery: MutableLiveData<List<Delivery>> = MutableLiveData(MockedData.homeDelivery)
}