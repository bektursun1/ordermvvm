package com.bektursun.ordermvvm.ui.fragment.home.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bektursun.ordermvvm.R
import com.bektursun.ordermvvm.data.model.Delivery
import com.bektursun.ordermvvm.databinding.ItemDeliveryBinding
import com.bektursun.ordermvvm.utils.getDrawableExt

class DeliveryCategoryAdapter : RecyclerView.Adapter<DeliveryCategoryAdapter.DeliveryVH>() {

    private var deliveryList: List<Delivery> = emptyList()

    fun submitItems(items: List<Delivery>) {
        deliveryList = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeliveryVH =
        DeliveryVH.from(parent)

    override fun onBindViewHolder(holder: DeliveryVH, position: Int) {
        holder.bind(deliveryList[position])
    }

    override fun getItemCount(): Int = deliveryList.size

    class DeliveryVH(private val binding: ItemDeliveryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(delivery: Delivery) {
            with(binding) {
                cpCategory.text = delivery.name
                val icon = delivery.icon
                if (icon != null) {
                    cpCategory.isCheckable = true
                    cpCategory.isChipIconVisible = true
                    //cpCategory.chipIcon = root.context?.getDrawableExt(R.drawable.ic_delivery)
                    cpCategory.setCheckedIconResource(icon)
                } else {
                    cpCategory.isChipIconVisible = false
                    cpCategory.isCheckable = true
                }
            }
        }

        companion object {
            fun from(parent: ViewGroup): DeliveryVH {
                val inflater = LayoutInflater.from(parent.context)
                val binding = ItemDeliveryBinding.inflate(inflater)
                return DeliveryVH(binding)
            }
        }

    }

}