package com.bektursun.ordermvvm.ui.fragment.pharmacy

import android.view.LayoutInflater
import com.bektursun.ordermvvm.core.ui.CoreFragment
import com.bektursun.ordermvvm.databinding.FragmentPharmacyBinding

class PharmacyFragment : CoreFragment<FragmentPharmacyBinding, PharmacyVM>(PharmacyVM::class) {

    override fun createVB(inflater: LayoutInflater): FragmentPharmacyBinding =
        FragmentPharmacyBinding.inflate(inflater)

    companion object {
        fun newInstance() = PharmacyFragment()
    }
}