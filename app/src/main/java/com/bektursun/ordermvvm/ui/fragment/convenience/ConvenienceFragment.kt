package com.bektursun.ordermvvm.ui.fragment.convenience

import android.view.LayoutInflater
import com.bektursun.ordermvvm.core.ui.CoreFragment
import com.bektursun.ordermvvm.databinding.FragmentConvenienceBinding

class ConvenienceFragment : CoreFragment<FragmentConvenienceBinding, ConvenienceVM>(ConvenienceVM::class) {

    override fun createVB(inflater: LayoutInflater): FragmentConvenienceBinding =
        FragmentConvenienceBinding.inflate(inflater)

    companion object {
        fun newInstance() = ConvenienceFragment()
    }
}