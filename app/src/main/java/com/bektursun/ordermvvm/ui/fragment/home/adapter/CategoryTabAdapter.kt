package com.bektursun.ordermvvm.ui.fragment.home.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bektursun.ordermvvm.ui.fragment.convenience.ConvenienceFragment
import com.bektursun.ordermvvm.ui.fragment.grocery.GroceryFragment
import com.bektursun.ordermvvm.ui.fragment.pharmacy.PharmacyFragment
import com.bektursun.ordermvvm.ui.fragment.takeaway.TakeawayFragment

class CategoryTabAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    private val tabFragmentsCreator: Map<Int, () -> Fragment> = mapOf(
        TAKEAWAYS_PAGE_INDEX to { TakeawayFragment.newInstance() },
        GROCERY_PAGE_INDEX to { GroceryFragment.newInstance() },
        CONVENIENCE_PAGE_INDEX to { ConvenienceFragment.newInstance() },
        PHARMACY_PAGE_INDEX to { PharmacyFragment.newInstance() }
    )

    override fun getItemCount(): Int = tabFragmentsCreator.size

    override fun createFragment(position: Int): Fragment =
        tabFragmentsCreator[position]?.invoke() ?: throw IndexOutOfBoundsException()

    companion object {
        const val TAKEAWAYS_PAGE_INDEX: Int = 0
        const val GROCERY_PAGE_INDEX: Int = 1
        const val CONVENIENCE_PAGE_INDEX: Int = 2
        const val PHARMACY_PAGE_INDEX: Int = 3
    }
}