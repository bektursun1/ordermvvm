package com.bektursun.ordermvvm.di

import com.bektursun.ordermvvm.ui.fragment.detail.ProductDetailVM
import com.bektursun.ordermvvm.ui.fragment.detail.ProductDetailVMImpl
import com.bektursun.ordermvvm.ui.fragment.grocery.GroceryVM
import com.bektursun.ordermvvm.ui.fragment.grocery.GroceryVMImpl
import com.bektursun.ordermvvm.ui.fragment.home.HomeVM
import com.bektursun.ordermvvm.ui.fragment.home.HomeVMImpl
import com.bektursun.ordermvvm.ui.fragment.takeaway.TakeawayVM
import com.bektursun.ordermvvm.ui.fragment.takeaway.TakeawayVMImpl
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel<HomeVM> { HomeVMImpl() }
    viewModel<TakeawayVM> { TakeawayVMImpl() }
    viewModel<GroceryVM> { GroceryVMImpl() }
    viewModel<ProductDetailVM> { ProductDetailVMImpl() }
}