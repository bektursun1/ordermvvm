package com.bektursun.ordermvvm.application

import android.app.Application
import com.bektursun.ordermvvm.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class OrderApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@OrderApplication)
            modules(appModule)
        }
    }
}