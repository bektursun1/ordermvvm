package com.bektursun.ui

import android.content.Context
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.bektursun.ui.databinding.SearchOrderBinding

class OrderSearch @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val vb: SearchOrderBinding = SearchOrderBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        context.theme.obtainStyledAttributes(attrs, R.styleable.OrderSearch, 0, 0).apply {
            vb.etSearch.hint = getString(R.styleable.OrderSearch_hint).toString()
            vb.ivFilter.isVisible = getBoolean(R.styleable.OrderSearch_showFilterIcon, true)
            recycle()
        }
    }

    fun setHint(@StringRes resId: Int) {
        vb.etSearch.setHint(resId)
    }

    fun setHint(text: String) {
        vb.etSearch.hint = text
    }

    fun setOnFilterListener(listener: OnClickListener) {
        vb.ivFilter.setOnClickListener(listener)
    }

    fun setOnSearchListener(listener: OnClickListener) {
        vb.ivSearch.setOnClickListener(listener)
    }

    fun setOnSearchEditTextListener(textWatcher: TextWatcher) {
        vb.etSearch.addTextChangedListener(textWatcher)
    }

}