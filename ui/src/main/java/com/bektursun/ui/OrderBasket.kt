package com.bektursun.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.view.isVisible
import com.bektursun.ui.databinding.OrderBasketBinding

class OrderBasket @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val vb: OrderBasketBinding =
        OrderBasketBinding.inflate(LayoutInflater.from(context), this, true)

    // TODO("change this with getString()")
    fun setCount(count: Float) {
        vb.tvCount.text = "$ $count"
    }

    fun showBasketIcon(isShow: Boolean) {
        vb.ivBasket.isVisible = isShow
    }

}