package com.bektursun.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.bektursun.ui.databinding.OrderCheckoutBinding

class OrderCheckout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val binding: OrderCheckoutBinding =
        OrderCheckoutBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        context.theme.obtainStyledAttributes(attrs, R.styleable.OrderCheckout, 0, 0).apply {
            recycle()
        }
    }

    fun setPrice(price: Float) {
        binding.priceBasket.setCount(price)
    }

    fun setCount(count: Float) {
        binding.basketCount.setCount(count)
    }

    fun showBasketIcon(isShow: Boolean) {
        binding.priceBasket.showBasketIcon(isShow)
    }
}