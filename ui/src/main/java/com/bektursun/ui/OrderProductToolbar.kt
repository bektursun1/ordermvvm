package com.bektursun.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.StringRes
import com.bektursun.ui.databinding.ToolbarProductOrderBinding
import com.google.android.material.appbar.AppBarLayout

class OrderProductToolbar(context: Context, attrs: AttributeSet) : AppBarLayout(context, attrs) {

    private val vb: ToolbarProductOrderBinding =
        ToolbarProductOrderBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        context.theme.obtainStyledAttributes(attrs, R.styleable.OrderProductToolbar, 0, 0).apply {
            vb.toolbar.title = getString(R.styleable.OrderProductToolbar_title).toString()
            vb.toolbar.navigationIcon = getDrawable(R.styleable.OrderProductToolbar_leftIcon)
            vb.ivCategoryIcon.setImageDrawable(getDrawable(R.styleable.OrderProductToolbar_category_icon))
            vb.tvCategoryName.text = getString(R.styleable.OrderProductToolbar_category_title)
            recycle()
        }
    }

    fun setToolbarTitle(@StringRes resId: Int) {
        vb.toolbar.setTitle(resId)
    }

    fun setToolbarTitle(text: String) {
        vb.toolbar.title = text
    }

    fun setCategoryTitle(@StringRes resId: Int) {
        vb.tvCategoryName.setText(resId)
    }

    fun setCategoryTitle(text: String) {
        vb.tvCategoryName.text = text
    }

    fun setNavListener(listener: OnClickListener) {
        vb.toolbar.setNavigationOnClickListener(listener)
    }

    fun setExpandMenuListener(listener: OnClickListener) {
        vb.ivExpandIcon.setOnClickListener(listener)
    }

}