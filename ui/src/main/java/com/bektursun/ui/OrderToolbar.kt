package com.bektursun.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.Toolbar
import com.bektursun.ui.databinding.ToolbarOrderBinding
import com.google.android.material.appbar.AppBarLayout

class OrderToolbar(context: Context, attrs: AttributeSet) : AppBarLayout(context, attrs) {

    private val vb: ToolbarOrderBinding =
        ToolbarOrderBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        context.theme.obtainStyledAttributes(attrs, R.styleable.OrderToolbar, 0, 0).apply {
            vb.toolbar.title = getString(R.styleable.OrderToolbar_title).toString()
            vb.toolbar.navigationIcon = getDrawable(R.styleable.OrderToolbar_leftIcon)
            recycle()
        }
    }

    fun setTitle(@StringRes resId: Int) {
        vb.toolbar.setTitle(resId)
    }

    fun setTitle(text: String) {
        vb.toolbar.title = text
    }

    fun setNavIcon(@DrawableRes resId: Int) {
        vb.toolbar.setNavigationIcon(resId)
    }

    fun setNavListener(listener: OnClickListener) {
        vb.toolbar.setNavigationOnClickListener(listener)
    }

    fun setMenuListener(listener: Toolbar.OnMenuItemClickListener) {
        vb.toolbar.setOnMenuItemClickListener(listener)
    }
}
